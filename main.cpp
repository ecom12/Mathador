#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* to avoid infinite loops */
#define MAX_DEPTH           100

#define MAX_TABLE_SIZE      10

#define RESULT_STRING_SIZE  128

#define OPERATOR_PLUS       1
#define OPERATOR_MINUS      2
#define OPERATOR_MUL        3
#define OPERATOR_DIV        4

#define RET_DONE            0
#define RET_ERROR           -1

#define MODE_USE_OPERATOR_ONCE  0x01


uint32_t nbTests = 0;
uint32_t nbWin   = 0;

inline void printTbl( int32_t *tbl, uint32_t tblSize ) {
return;
    printf( "[" );
    for ( int i = 0; i < tblSize; i++) {
        if ( i == tblSize-1 ) {
            printf("%d", tbl[i]);
        } else {
            printf("%d,", tbl[i]);
        }
    }
    printf( "], size=%d\n", tblSize );
}

int32_t runMathador( int32_t goal, uint32_t mode, int32_t *tbl, uint32_t tblSize, uint32_t *operators, uint32_t operatorSize, char *resString ) {
    char resultString[RESULT_STRING_SIZE] = "";

    int32_t *newTbl = NULL;
    uint32_t newTblSize = tblSize-1;

    uint32_t *newOperators = NULL;
    uint32_t newOperatorSize = ( (mode & MODE_USE_OPERATOR_ONCE) == 1 ) ? operatorSize -1 : operatorSize;

    char str[RESULT_STRING_SIZE] = "";
    char newResStr[RESULT_STRING_SIZE] = "";

    int32_t res = 0;

    printTbl( tbl, tblSize );
//    printf( "[DBG] resString: %s\n", resString );

    if ( tblSize == 1 ) {
        nbTests++;
        if ( goal == tbl[0] ) {
            nbWin++;
            printf( "Goal: %d = %s\n", goal, resString );
//            printf( "======== Gagné ========\n\n" );
        } else {
//            printf( "Goal: %d = %s\n", goal, resString );
//            printf( "======== Perdu ========\n\n" );
        }
        return RET_DONE;
    } else {
        newTbl = (int32_t *)malloc( newTblSize * sizeof(int32_t) );
    }

    newOperators = (uint32_t *)malloc( newOperatorSize * sizeof(uint32_t) );

    for ( int op1 = 0; op1 < tblSize; op1++ ) {
        for ( int op2 = 0; op2 < tblSize; op2++ ) {
            if ( op2 != op1 ) {
                for ( int i = 0; i < operatorSize; i++ ) {
                    bool skip = false;
                    newResStr[0] = 0;

//                    printf( "[DBG] op1=%d, op2=%d, operator=%d\n", op1, op2, i);

                    switch ( operators[i] ) {
                        case OPERATOR_PLUS:
                            if ( op1 > op2 ) {
                                /* order of operation does not matter */
                                skip = true;
                            } else {
                                res = tbl[op1] + tbl[op2];
                                sprintf( str, "(%d + %d) = %d ==> ", tbl[op1], tbl[op2], res );
                            }
                            break;

                        case OPERATOR_MINUS:
                            res = tbl[op1] - tbl[op2];
                            sprintf( str, "(%d - %d) = %d ==> ", tbl[op1], tbl[op2], res );
                            break;

                        case OPERATOR_MUL:
                            if ( op1 > op2 ) {
                                /* order of operation does not matter */
                                skip = true;
                            } else {
                                res = tbl[op1] * tbl[op2];
                                sprintf( str, "(%d * %d) = %d ==> ", tbl[op1], tbl[op2], res );
                            }
                            break;

                        case OPERATOR_DIV:
                            if (tbl[op2] == 0 ) {
                                skip = true;
                            } else {
                                res = tbl[op1] / tbl[op2];
                                if ( res * tbl[op2] != tbl[op1] ) {
                                    skip = true;
//                                    printf( "SKIP (%d / %d) = %.4f \n", tbl[op1], tbl[op2], (float)(tbl[op1]) / (float)(tbl[op2]) );
                                } else {
                                    sprintf( str, "(%d / %d) = %d ==> ", tbl[op1], tbl[op2], res );
                                }
                            }
                            break;

                        default:
                            printf( "[ERROR] Unknown operator (%d), aborting.\n", operators[i] );
                            free( newTbl );
                            free( newOperators );
                            newTbl = NULL;
                            newOperators = NULL;
                            return RET_ERROR;
                    }

                    if ( skip ==false ) {
//                        printf( "[DBG] res=%d, newTblSize=%d\n", res, newTblSize);

                        newTbl[0] = res;
                        int n = 1;
                        for ( int j = 0; j < tblSize; j++ ) {
                            if ( j != op1 && j != op2 ) {
                                if ( n < newTblSize ) {
                                    newTbl[n] = tbl[j];
                                    n++;
                                } else {
                                    printf( "[ERROR] out of bound\n" );
                                    free( newTbl );
                                    free( newOperators );
                                    newTbl = NULL;
                                    newOperators = NULL;
                                    return RET_ERROR;
                                }
                            }
                        }

                        n = 0;
                        for ( int j = 0; j < operatorSize; j++ ) {
                            if ( operators[j] != operators[i] ) {
                                newOperators[n] = operators[j];
                                n++;
                            } else {
                                if ( (mode & MODE_USE_OPERATOR_ONCE) != MODE_USE_OPERATOR_ONCE ) {
                                    newOperators[n] = operators[j];
                                    n++;
                                }
                            }
                        }
/*                        printf( "A %s\n", resString );
                        printf( "B %s\n", newResStr );
                        printf( "C %s\n", str );
*/
                        strcat( newResStr, resString );
//                        printf( "BA %s\n", newResStr );

                        strcat( newResStr, str );
//                        printf( "BAC %s\n", newResStr );
                        runMathador( goal, mode, newTbl, newTblSize, newOperators, newOperatorSize, newResStr );
                    }
                }
            }
        }
    }

    free( newTbl );
    free( newOperators );
    newTbl = NULL;
    newOperators = NULL;

    return RET_DONE;
}

int32_t readConfig( int32_t *goal, uint32_t *mode, int32_t *tbl, uint32_t tblSize ) {
    printf( "Entre le nombre à trouver: " );
    scanf( "%d", goal );

    for ( int i = 0; i < tblSize; i++ ) {
        printf( "Entre le nombre #%d: ", i);
        scanf( "%d", &tbl[i] );
    }
    printf( "Peut-on utiliser les operateurs plusieurs fois? (0=non, 1=oui): " );
    uint32_t m = 0;
    scanf( "%d", &m );
    *mode = 0;
    if ( m == 0 ) {
        *mode = (*mode | MODE_USE_OPERATOR_ONCE );
    } else {
        *mode = *mode & (!MODE_USE_OPERATOR_ONCE);
    }

    printf( "\n\n====\n\nTu as choisi de trouver le nombre %d avec {", *goal );
    for ( int i = 0; i < tblSize; i++ ) {
        printf( "%d,", tbl[i] );
    }
    printf( "}\n" );
    if ( (*mode & MODE_USE_OPERATOR_ONCE) == MODE_USE_OPERATOR_ONCE ) {
        printf( "Chaque operateur ne sera utilisé qu'une seule fois.\n" );
    } else {
        printf( "Chaque operateur pourra être utilisé plusieurs fois.\n" );
    }

    printf( "\n\nOn y va ? (0=non, 1=oui): " );
    uint32_t go;
    scanf( "%d", &go );

    printf( "====\n\n" );
    if ( go == 1) {
        return RET_DONE;
    }
    return RET_ERROR;
}

int main() {
    /* Allocate resources */
    char resultString[RESULT_STRING_SIZE] = "";

    uint32_t tblSize = 5;
    int32_t *tbl = (int32_t *)malloc( tblSize * sizeof(int32_t) );

    uint32_t opTblSize = 4;
    uint32_t *opTbl = (uint32_t *)malloc( opTblSize * sizeof(uint32_t) );

    uint32_t mode = MODE_USE_OPERATOR_ONCE;

    /* Initialize data */
    opTbl[0] = OPERATOR_PLUS;
    opTbl[1] = OPERATOR_MINUS;
    opTbl[2] = OPERATOR_MUL;
    opTbl[3] = OPERATOR_DIV;

    tbl[0]   = 3;
    tbl[1]   = 2;
    tbl[2]   = 4;
    tbl[3]   = 12;
    tbl[4]   = 7;

    int32_t goal = 83;

    int res = readConfig(
        &goal,
        &mode,
        tbl,
        tblSize );

    if ( res == RET_DONE ) {
        printf( "C'EST PARTI !!!\n\n" );

        /* Run the Mathador ! */
        int32_t res = runMathador(
            goal,
            mode,
            tbl,
            tblSize,
            opTbl,
            opTblSize,
            resultString
        );

        printf( "\n\n nbWin / nbTotal: %d / %d\n\n", nbWin, nbTests );
    }

    /* Free resources */
    free(opTbl);
    opTbl = NULL;


    free(tbl);
    tbl = NULL;

    printf( "Done !" );
    return 0;
}