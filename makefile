# Define required macros here
SHELL   = /bin/sh

OBJS    = main.o
CFLAG   = -Wall -g
CC      = gcc
INCLUDE =
LIBS    = -lm

all: mathador

mathador:${OBJS}
	${CC} ${CFLAGS} ${INCLUDES} -o $@ ${OBJS} ${LIBS}

clean:
	-rm -f *.o core *.core

%.o: %.c
	${CC} ${CFLAGS} ${INCLUDES} -c $< -o $@